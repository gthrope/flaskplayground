import os
from flask import Flask

# initialization
application = Flask(__name__)
application.config.update(
    DEBUG = True,
)

#controllers
@application.route("/")
def hello():
    return "Hello from my playground!"

@application.route("/albert")
def albert():
    return "SUP ALBERT"

#launch
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    application.run(host='0.0.0.0', port=port)
